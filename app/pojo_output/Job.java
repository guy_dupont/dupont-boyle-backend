import java.util.Date;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Job
    extends BaseModel
{

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("gear_def_id")
    @Expose
    private String gearDefId;
    /**
     * gear instance this has been scheduled to
     * 
     */
    @SerializedName("gear_id")
    @Expose
    private String gearId;
    @SerializedName("scheduled_at")
    @Expose
    private Date scheduledAt;
    @SerializedName("file_size")
    @Expose
    private Object fileSize;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("completed_at")
    @Expose
    private Date completedAt;

    /**
     * 
     * @return
     *     The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The gearDefId
     */
    public String getGearDefId() {
        return gearDefId;
    }

    /**
     * 
     * @param gearDefId
     *     The gear_def_id
     */
    public void setGearDefId(String gearDefId) {
        this.gearDefId = gearDefId;
    }

    /**
     * gear instance this has been scheduled to
     * 
     * @return
     *     The gearId
     */
    public String getGearId() {
        return gearId;
    }

    /**
     * gear instance this has been scheduled to
     * 
     * @param gearId
     *     The gear_id
     */
    public void setGearId(String gearId) {
        this.gearId = gearId;
    }

    /**
     * 
     * @return
     *     The scheduledAt
     */
    public Date getScheduledAt() {
        return scheduledAt;
    }

    /**
     * 
     * @param scheduledAt
     *     The scheduled_at
     */
    public void setScheduledAt(Date scheduledAt) {
        this.scheduledAt = scheduledAt;
    }

    /**
     * 
     * @return
     *     The fileSize
     */
    public Object getFileSize() {
        return fileSize;
    }

    /**
     * 
     * @param fileSize
     *     The file_size
     */
    public void setFileSize(Object fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The completedAt
     */
    public Date getCompletedAt() {
        return completedAt;
    }

    /**
     * 
     * @param completedAt
     *     The completed_at
     */
    public void setCompletedAt(Date completedAt) {
        this.completedAt = completedAt;
    }

}
