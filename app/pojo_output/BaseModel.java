import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseModel {

    /**
     * UUID for this instance
     * 
     */
    @SerializedName("_id")
    @Expose
    private String id;

    /**
     * UUID for this instance
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * UUID for this instance
     * 
     * @param id
     *     The _id
     */
    public void setId(String id) {
        this.id = id;
    }

}
