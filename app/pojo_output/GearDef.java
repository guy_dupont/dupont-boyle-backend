import java.util.HashMap;
import java.util.Map;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Type of gear
 * 
 */
public class GearDef
    extends BaseModel
{

    /**
     * Name of the manufacturer.
     * (Required)
     * 
     */
    @SerializedName("make")
    @Expose
    private String make;
    /**
     * Name/number of the model
     * (Required)
     * 
     */
    @SerializedName("model")
    @Expose
    private String model;
    /**
     * product image url
     * 
     */
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    /**
     * Type of processor
     * (Required)
     * 
     */
    @SerializedName("type")
    @Expose
    private GearDef.Type type;

    /**
     * Name of the manufacturer.
     * (Required)
     * 
     * @return
     *     The make
     */
    public String getMake() {
        return make;
    }

    /**
     * Name of the manufacturer.
     * (Required)
     * 
     * @param make
     *     The make
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * Name/number of the model
     * (Required)
     * 
     * @return
     *     The model
     */
    public String getModel() {
        return model;
    }

    /**
     * Name/number of the model
     * (Required)
     * 
     * @param model
     *     The model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * product image url
     * 
     * @return
     *     The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * product image url
     * 
     * @param imageUrl
     *     The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Type of processor
     * (Required)
     * 
     * @return
     *     The type
     */
    public GearDef.Type getType() {
        return type;
    }

    /**
     * Type of processor
     * (Required)
     * 
     * @param type
     *     The type
     */
    public void setType(GearDef.Type type) {
        this.type = type;
    }

    public enum Type {

        @SerializedName("Compressor")
        COMPRESSOR("Compressor"),
        @SerializedName("EQ")
        EQ("EQ"),
        @SerializedName("Reverb")
        REVERB("Reverb"),
        @SerializedName("Delay")
        DELAY("Delay"),
        @SerializedName("Distortion")
        DISTORTION("Distortion"),
        @SerializedName("Other")
        OTHER("Other");
        private final String value;
        private final static Map<String, GearDef.Type> CONSTANTS = new HashMap<String, GearDef.Type>();

        static {
            for (GearDef.Type c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public String value() {
            return this.value;
        }

        public static GearDef.Type fromValue(String value) {
            GearDef.Type constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
