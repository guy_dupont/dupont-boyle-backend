import java.util.Date;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * An a piece of gear, owned by a studio
 * 
 */
public class Gear
    extends BaseModel
{

    /**
     * UUID for the studio that owns this gear
     * (Required)
     * 
     */
    @SerializedName("studio_id")
    @Expose
    private String studioId;
    /**
     * UUID for the type of this gear.  Will contain make, model, type, etc.
     * (Required)
     * 
     */
    @SerializedName("gear_def_id")
    @Expose
    private String gearDefId;
    /**
     * Optional name for this particular piece.
     * 
     */
    @SerializedName("name")
    @Expose
    private String name;
    /**
     * IP Address of this piece of gear
     * 
     */
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("updated_at")
    @Expose
    private Date updatedAt;

    /**
     * UUID for the studio that owns this gear
     * (Required)
     * 
     * @return
     *     The studioId
     */
    public String getStudioId() {
        return studioId;
    }

    /**
     * UUID for the studio that owns this gear
     * (Required)
     * 
     * @param studioId
     *     The studio_id
     */
    public void setStudioId(String studioId) {
        this.studioId = studioId;
    }

    /**
     * UUID for the type of this gear.  Will contain make, model, type, etc.
     * (Required)
     * 
     * @return
     *     The gearDefId
     */
    public String getGearDefId() {
        return gearDefId;
    }

    /**
     * UUID for the type of this gear.  Will contain make, model, type, etc.
     * (Required)
     * 
     * @param gearDefId
     *     The gear_def_id
     */
    public void setGearDefId(String gearDefId) {
        this.gearDefId = gearDefId;
    }

    /**
     * Optional name for this particular piece.
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * Optional name for this particular piece.
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * IP Address of this piece of gear
     * 
     * @return
     *     The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * IP Address of this piece of gear
     * 
     * @param address
     *     The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
