const dgram = require('dgram');
const Rx = require('rx');

/**
 *
 * @param userDatastore
 * @param jobDatastore
 * @constructor
 */
function UdpNotifier(userDatastore, jobDatastore) {
    this.userDatastore = userDatastore;
    this.jobDatastore = jobDatastore;
    this.client = dgram.createSocket('udp4');
}

UdpNotifier.prototype.pokeGear = function (gear) {
    console.log("NOTIFY GEAR: " + gear.address);
    var message = this.buildGearPokeBuffer();
    this.client.send(message, 0, message.length, 9099, gear.address, function (err) {
        if (err) {
            console.log(err);
        }
    });
};

UdpNotifier.prototype.updateClientProgress = function (jobId, processed, total) {
    var self = this;
    Rx.Observable
        .fromCallback(self.jobDatastore.getJobForId.bind(self.jobDatastore))(jobId)
        .flatMap(function (job) {
            return Rx.Observable.fromCallback(self.userDatastore.getUserById.bind(self.userDatastore)(job.user_id));
        })
        .subscribe(function (user) {
            // TODO
            console.log("NOTIFY: " + user.toString());
        });
};

UdpNotifier.prototype.alertClientFinished = function (job) {
    var self = this;
    this.userDatastore.getUserById(job.user_id, function (user) {
        var message = self.buildClientFinishedBuffer(job.getId());
        console.log("NOTIFY JOB FINISHED: " + job.getId());
        self.client.send(message, 0, message.length, 9099, user.last_address, function (err) {
            if (err) {
                console.log(err);
            }
        });
    });
};

UdpNotifier.prototype.buildGearPokeBuffer = function () {
    return new Buffer([0x02]);
};

UdpNotifier.prototype.buildClientFinishedBuffer = function (jobId) {
    var buffer = new Buffer(128);
    buffer[0] = 0x01;
    buffer[1] = jobId.length;
    buffer.write(jobId, 2, jobId.length);
    return buffer;
};

module.exports = UdpNotifier;