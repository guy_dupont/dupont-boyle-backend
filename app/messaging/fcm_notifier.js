var FCM = require('fcm-node');
const SERVER_KEY = "AAAAJiiQn-4:APA91bEH-I1rtvz99ip5f9UfkJpHHgeepXYOREZPCqeJJxRzBb3CqibkDh6UgiFfEfHq7oGi7INRNviJsmJxVv6nPefd1ZpZ1W1viow7R_ACCiDmMdsdzUxr3XziDZk8uG5SOHCLCy6XXUBOSFWONGDhTJhCuDGLdA";


/**
 *
 * @param userDatastore
 * @param jobDatastore
 * @constructor
 */
function FcmNotifier(userDatastore, jobDatastore) {
    this.userDatastore = userDatastore;
    this.jobDatastore = jobDatastore;
    this.fcm = new FCM(SERVER_KEY);
}

FcmNotifier.prototype.pokeGear = function (gear) {
    var dataObj =JSON.stringify(
        {
            "type" : 2
        }
    );
    var token = gear.fcm_token;
    if (!token || token == "undefined") {
        return;
    }
    var message = {
        to: token,
        data: {
            obj: dataObj,
        }
    };

    this.fcm.send(message, function(err, response){
        if (err) {
            console.log("Something has gone wrong!");
        } else {
            console.log("Successfully poked gear with response: ", response);
        }
    });

};

FcmNotifier.prototype.updateClientProgress = function (jobId, processed, total) {

};

FcmNotifier.prototype.sendStreamAddressToClient = function (fcmToken, gearAddress) {
    var self = this;
    var dataObj =JSON.stringify(
        {
            "type" : 3,
            "address" : gearAddress
        }
    );

    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
        to: fcmToken,
        data: {  //you can send only notification or only data(or include both)
            obj: dataObj,
        }
    };
    console.log(fcmToken);
    self.fcm.send(message, function(err, response){
        if (err) {
            console.log("Something has gone wrong!");
        } else {
            console.log("Successfully sent with response: ", response);
        }
    });
};

FcmNotifier.prototype.alertClientFinished = function (job) {
    var self = this;
    var dataObj =JSON.stringify(
        {
            "type" : 1,
            "job_id" : job.getId()
        }
    );

    this.userDatastore.getUserById(job.user_id, function (user) {
        var token = user.fcm_token;
        if (!token || token == "undefined") {
            console.log("user does not have an fcm token");
            return;
        }
        var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            to: token,
            data: {  //you can send only notification or only data(or include both)
                obj: dataObj,
            }
        };

        self.fcm.send(message, function(err, response){
            if (err) {
                console.log("Something has gone wrong!");
            } else {
                console.log("Successfully sent with response: ", response);
            }
        });
    });


};

module.exports = FcmNotifier;