const Job = require('./model/job');
const Gear = require('./model/gear');
const Uuid = require('uuid');
const aws = require('aws-sdk');
const STREAM_AVERAGE_WEIGHT = 0.1;

/**
 *
 * @param {GearDefDatastore} gearDefDatastore
 * @param {GearDatastore} gearDatastore
 * @param {JobDatastore} jobDatastore
 * @param {UserDatastore} userDatastore
 * @param {*} udpNotifier
 * @param {*} fileSystem
 * @param {Scheduler} scheduler
 * @constructor
 */
function WebInterface(gearDefDatastore, gearDatastore, jobDatastore, userDatastore, udpNotifier, fileSystem, scheduler) {
    this.gearDefDatastore = gearDefDatastore;
    this.gearDatasotre = gearDatastore;
    this.fileSystem = fileSystem;
    this.jobDatastore = jobDatastore;
    this.userDatastore = userDatastore;
    this.udpNotifier = udpNotifier;
    this.scheduler = scheduler;
    // bias towards streaming
    this.streamingBias = 0;
    this.numberOfStreamingGear = 0;
    this.numberOfNormalGear = 0;

}

/**
 *
 * @callback requestCallback
 * @param {number} responseCode
 * @param {object} responseMessage
 */

/**
 *
 * @param {requestCallback} callback
 */
WebInterface.prototype.onRootRequest = function (callback) {
    callback(200, {"Sample JSON": "Hello World"});
};

/**
 *
 * @param {requestCallback} callback
 */
WebInterface.prototype.getAllGearDef = function (callback) {
    try {
        callback(200, this.gearDefDatastore.getAll());
    }
    catch (e) {
        callback(500, {"error": "Error fetching gear definitions"})
    }
};

/**
 *
 * @param {requestCallback} callback
 * @param {string} id
 */
WebInterface.prototype.getGearDefById = function (callback, id) {
    try {
        var gearDef = this.gearDefDatastore.getById(id);
        if (gearDef) {
            callback(200, gearDef);
        } else {
            callback(404, {"error": "Gear definition not found"});
        }
    }
    catch (e) {
        callback(500, ({"error": "Error fetching gear definition by id"}));
    }
};

WebInterface.prototype.updateUserToken = function (callback, requestObject) {
    var userId = requestObject.user_id;
    var address = requestObject.address;
    var fcmToken = requestObject.fcm_token;
    var errMessage;
    if (!userId) {
        errMessage = "Invalid user id";
    } else if (!fcmToken) {
        errMessage = "Invalid fcm token";
    }
    if (errMessage) {
        callback(400, {"error": errorMessage});
        return;
    }
    this.userDatastore.insertOrUpdate(userId, address, fcmToken);
    callback(204);
};

WebInterface.prototype.markStreamAsCompleted = function (callback, requestObject) {
    var jobId = requestObject.job_id;
    var errMessage;
    if (!jobId) {
        errMessage = "Invalid job id";
    }

    if (errMessage) {
        callback(400, {"error": errorMessage});
        return;
    }

    this.jobDatastore.markAsStreamComplete(jobId);
    callback(204);

};

WebInterface.prototype.updateGearToken = function (callback, requestObject) {
    var gearId = requestObject.gear_id;
    var address = requestObject.address;
    var fcmToken = requestObject.fcm_token;
    var self = this;
    var errMessage;
    if (!gearId) {
        errMessage = "Invalid user id";
    } else if (!fcmToken) {
        errMessage = "Invalid fcm token";
    }
    if (errMessage) {
        callback(400, {"error": errorMessage});
        return;
    }
    this.gearDatasotre.getGearById(gearId, function (gear) {
        if (!gear) {
            callback(400, {"error": "Gear is not registered"});
            return;
        }
        gear.address = address;
        gear.fcm_token = fcmToken;
        self.gearDatasotre.insertOrUpdate(gear);
        callback(204);
    });

};

WebInterface.prototype.getAllJobsForUser = function (callback, requestObject) {
    var userId = requestObject.user_id;
    if (!userId) {
        callback(400, {"error": "Invalid user id"});
        return;
    }
    this.jobDatastore.getJobsForUser(userId, function (jobs) {
        callback(200, {"jobs": jobs});
    })
};

WebInterface.prototype.shouldNewGearStream = function () {
    var gearRatio = this.numberOfStreamingGear / (this.numberOfStreamingGear + this.numberOfNormalGear);
    var adjustedBias = (this.streamingBias + 2) / 4;
    if (adjustedBias >= gearRatio) {
        this.numberOfStreamingGear++;
        return true;
    } else {
        this.numberOfNormalGear++;
        return false;
    }
};

WebInterface.prototype.insertOrUpdateGear = function (callback, requestObject) {
    var errorMessage;
    var studioId = requestObject.studio_id;
    var gearDefId = requestObject.gear_def_id;
    var address = requestObject.address;
    var fcmToken = requestObject.fcm_token;

    // optional
    var id = requestObject.id;
    var name = requestObject.name;

    if (!studioId) {
        errorMessage = "Invalid studio id";
    } else if (!gearDefId || !this.gearDefDatastore.getById(gearDefId)) {
        errorMessage = "Invalid Gear Def";
    } else if (!address) {
        errorMessage = "Unable to parse address";
    }

    if (errorMessage) {
        callback(400, {"error": errorMessage});
        return;
    }

    if (!id) {
        id = Uuid.v1();
    }

    var stream = this.shouldNewGearStream();
    var gear = new Gear(id, studioId, gearDefId, name, fcmToken, stream, address);
    this.gearDatasotre.insertOrUpdate(gear);
    console.log("gear registered: " + gear.getId() + ", streaming = " + stream);
    callback(200, gear);
};

WebInterface.prototype.getNextJobForGear = function (callback, gearId) {
    var self = this;
    this.jobDatastore.getNextJobForGear(gearId, function (job) {
        if (job && job.user_id) {
            self.userDatastore.getUserById(job.user_id, function (user) {
                if (user) {
                    callback(200, job, user.fcm_token);
                } else {
                    console.log("error: unable to find user for job");
                    callback(200);
                }
            })
        } else {
            callback(200);
        }

    });
};

WebInterface.prototype.downloadFileForJob = function (callback, fileNameFunction, requestObject) {
    var self = this;
    var jobId = requestObject.job_id;
    if (!jobId) {
        callback(400, {"error": "Invalid job ID"});
        return;
    }

    this.jobDatastore.getJobForId(jobId, function (job) {
        if (!job) {
            callback(400, {"error": "Unable to locate job"});
            return;
        }
        self.fileSystem.getJobReadStream(job, fileNameFunction, function (err, stream) {
            if (err) {
                callback(404, {"error": "File not found"});
            } else {
                callback(200, stream);
            }
        });
    });
};

/**
 *
 * @param {requestCallback} callback
 * @param {Object} requestObject
 */
WebInterface.prototype.uploadCompleted = function (callback, requestObject) {
    var errorMessage;
    var dataStore = this.jobDatastore;
    var fileSystem = this.fileSystem;
    var jobId = requestObject.job_id;
    var audioFile = requestObject.audio;
    var self = this;

    if (!audioFile) {
        errorMessage = "No file uploaded"
    } else if (!jobId) {
        errorMessage = "Invalid job ID";
    }

    if (errorMessage) {
        callback(400, {"error": errorMessage});
        return;
    }

    dataStore.getJobForId(jobId, function (job) {
        if (!job || !job.user_id) {
            errorMessage = "No job found with id: " + jobId;
            callback(400, {"error": errorMessage});
            return;
        }

        if (!fileSystem.userDirectoryExists(job.user_id)) {
            callback(400, {"error": "Owner for this job no longer available."});
            return;
        }

        fileSystem.saveUploadedFile(job.user_id, job.getAltId(), audioFile, function (err) {
            if (err) {
                callback(500, {"error": err});
            }
            else {
                var completeDate = dataStore.markAsComplete(job.getId());
                job.setCompletedAt(completeDate);
                self.udpNotifier.alertClientFinished(job);
                callback(200, job);
            }
        });

    });
};

/**
 *
 * @param {boolean} stream
 */
WebInterface.prototype.updateStreamRatio = function (stream) {
    var movement = stream ? 1 : -1;
    this.streamingBias = (1 - STREAM_AVERAGE_WEIGHT) * this.streamingBias + STREAM_AVERAGE_WEIGHT * movement;
    console.log("streaming bias: " + this.streamingBias);

};

/**
 *
 * @param {requestCallback} callback
 * @param {Object} requestObject
 */
WebInterface.prototype.uploadAudio = function (callback, requestObject) {
    var errorMessage;
    var userId = requestObject.user_id;
    var gearDefId = requestObject.gear_def_id;
    var audioFile = requestObject.audio;
    var address = requestObject.address;
    var fcmToken = requestObject.fcm_token;
    var shouldStream = requestObject.stream == 'true';
    this.updateStreamRatio(shouldStream);

    if (!audioFile) {
        errorMessage = "No file uploaded"
    } else if (!userId) {
        errorMessage = "Invalid User ID";
    } else if (!gearDefId || !this.gearDefDatastore.getById(gearDefId)) {
        errorMessage = "Invalid Gear Def";
    } else if (!address) {
        errorMessage = "Invalid Address";
    }

    if (errorMessage) {
        callback(400, {"error": errorMessage});
        return;
    }

    var audioSize = audioFile.data.length;
    var job = new Job(Uuid.v1(), userId, gearDefId, audioSize, requestObject.name, shouldStream);

    this.fileSystem.createUserDirectory(userId);
    this.fileSystem.saveUploadedFile(userId, job.getId(), audioFile, function (err) {
        if (err) {
            callback(500, {"error": err});
        }
        else {
            callback(200, job);
        }
    });

    this.jobDatastore.insertNew(job);
    this.userDatastore.insertOrUpdate(job.user_id, address, fcmToken);
    this.scheduler.poke();
};

module.exports = WebInterface;
