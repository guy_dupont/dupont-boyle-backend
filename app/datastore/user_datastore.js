/**
 *
 * @param db
 * @constructor
 */
function UserDatastore(db) {
    this.db = db;
    var dateString = db.getDateString();
    this.db.init([
        'CREATE TABLE IF NOT EXISTS userz (id TEXT PRIMARY KEY, last_address TEXT, fcm_token TEXT, updated_at ' + dateString + ')',
        'DELETE FROM userz'
    ]);
    // clear the db every time?
}

/**
 *
 * @param {String} userId
 * @param {String} address
 * @param {String} fcmToken
 */
UserDatastore.prototype.insertOrUpdate = function (userId, address, fcmToken) {
    var date = new Date().toISOString();
    this.db.upsert("userz", ["id", "last_address", "fcm_token", "updated_at"], [userId, address, fcmToken, date]);
};

UserDatastore.prototype.getUserById = function (userId, callback) {
    this.db.get("SELECT * FROM userz WHERE id = '" + userId + "'", function (err, row) {
        callback(row);
    });
};

module.exports = UserDatastore;