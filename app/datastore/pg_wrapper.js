/**
 * Created by GuyIntrepid on 11/22/16.
 */
/**
 *
 * @param db
 * @constructor
 */
function PgWrapper(db) {
    this.db = db;
}

PgWrapper.prototype.get = function (statement, callback) {
    this.all(statement, function (err, rows) {
        callback(err, rows[0]);
    })
};

PgWrapper.prototype.all = function (statement, callback) {
    var query = this.db.query(statement);
    query.on('error', function (err) {
        callback(err);
    });
    query.on('row', function (row, result) {
        result.addRow(row);
    });
    query.on('end', function (result) {
        if (!result.rows) {
            result.rows = [];
        }
        callback(null, result.rows);
    });
};

PgWrapper.prototype.run = function (statement, values, callback) {
    var vs = [];
    for (var key in values) {
        if (Object.prototype.hasOwnProperty.call(values, key)) {
            vs.push(values[key]);
        }
    }
    this.db.query(statement, vs, callback);
};

PgWrapper.prototype.upsert = function (tableName, cols, values) {
    var qString = this.buildQString(cols, false);
    var qString2 = this.buildQString(values, true);
    var statement = "UPDATE " + tableName + " SET " + qString + " = " + qString2 + " WHERE " + cols[0] + " = '" + values[0] + "'";
    var self = this;
    this.db.query('BEGIN', function (err, result) {
        self.db.query(statement, function(err, result) {
            if (err) {
                console.log(err);
            }
            if (result.rowCount < 1) {
                var insertStatement = "INSERT INTO " + tableName + qString + " VALUES" + qString2;
                self.db.query(insertStatement, function (err, result) {
                    self.db.query('COMMIT', function (err, result) {
                        if (err) {
                            console.log(err);
                        }
                    })
                });
            } else {
                self.db.query('COMMIT', function (err, result) {
                    if (err) {
                        console.log(err);
                    }
                })
            }
        });
    })

};

PgWrapper.prototype.buildQString = function (vals, shouldUseQuotes) {
    var qString = '(';
    for (var i = 0; i < vals.length; i++) {
        if (i > 0) {
            qString += ', '
        }
        var val = vals[i];
        if (shouldUseQuotes && typeof val != 'number') {
            val = "'" + val + "'";
        }
        qString += val;
    }
    qString += ')';
    return qString;
};

PgWrapper.prototype.init = function (statementList) {
    for (var i = 0; i < statementList.length; i++) {
        this.db.query(statementList[i]);
    }
};

PgWrapper.prototype.getDateString = function () {
    return "TIMESTAMP";
};

module.exports = PgWrapper;