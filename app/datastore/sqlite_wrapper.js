/**
 * Created by GuyIntrepid on 11/22/16.
 */
/**
 *
 * @param {sqlite3.Database} db
 * @constructor
 */
function SqliteWrapper(db) {
    this.db = db;
}

SqliteWrapper.prototype.get = function (statement, callback) {
    this.db.get(statement, callback);
};

SqliteWrapper.prototype.all = function (statement, callback) {
    this.db.all(statement, callback);
};

SqliteWrapper.prototype.run = function (statement, values, callback) {
    this.db.run(statement, values, callback);
};

SqliteWrapper.prototype.init = function (statementList) {
    var self = this;
    self.db.serialize(function () {
        for (var i = 0; i < statementList.length; i++) {
            self.db.run(statementList[i]);
        }
    })
};

SqliteWrapper.prototype.upsert = function (tableName, cols, values) {
    var colStr = cols.join(", ");
    var qString = new Array(cols.length).fill("?").join(", ");
    var statement = this.db.prepare("INSERT OR REPLACE INTO " + tableName + " (" + colStr + ") VALUES ("+qString+")");
    statement.run(values);
};

SqliteWrapper.prototype.getDateString = function () {
    return "DATETIME";
};

module.exports = SqliteWrapper;