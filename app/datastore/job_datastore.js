const Job = require('../model/job');
/**
 * @param db
 * @constructor
 */
function JobDatastore(db) {
    this.db = db;
    var dateString = db.getDateString();
    this.db.init([
        'CREATE TABLE IF NOT EXISTS job (id TEXT PRIMARY KEY, user_id TEXT, gear_def_id TEXT, gear_id TEXT, file_size INTEGER, name TEXT, stream BOOLEAN, scheduled_at ' + dateString + ', completed_at ' + dateString +')',
        'DELETE FROM job'
    ]);
    // clear the db every time?
}

/**
 *
 * @param {Job} job
 */
JobDatastore.prototype.insertNew = function (job) {
    var id = job.getId();
    this.db.run("INSERT INTO job (id, gear_def_id, user_id, file_size, name, stream) VALUES ($1, $2, $3, $4, $5, $6)", {
        $1: id,
        $2: job.gear_def_id,
        $3: job.user_id,
        $4: job.file_size,
        $5: job.name,
        $6: job.stream
    });
};

JobDatastore.prototype.getJobForId = function (jobId, callback) {
    this.db.get("SELECT * FROM job WHERE id = '" + jobId + "'", this._rowMapper(callback));
};

JobDatastore.prototype.getJobsForUser = function (userId, callback) {
    this.db.all("SELECT * FROM job WHERE user_id = '" + userId + "'", this._rowMapper(callback));
};

JobDatastore.prototype.getNextJobForGear = function (gearId, callback) {
    this.db.get("SELECT * FROM job WHERE gear_id = '" + gearId + "' AND completed_at IS NULL ORDER BY scheduled_at", this._rowMapper(callback));
};

JobDatastore.prototype.markAsScheduled = function (jobId, gearId) {
    var date = new Date().toISOString();
    this.db.run("UPDATE job SET gear_id=$1, scheduled_at=$2 WHERE id = $3", {
        $1: gearId,
        $2: date,
        $3: jobId
    });
    return date;
};

JobDatastore.prototype.markAsComplete = function (jobId) {
    var date = new Date().toISOString();
    this.db.run("UPDATE job SET completed_at=$1 WHERE id = $2", {
        $1: date,
        $2: jobId
    });
    return date;
};

JobDatastore.prototype.markAsStreamComplete = function (jobId) {
    var date = new Date().toISOString();
    this.db.run("UPDATE job SET completed_at=$1, stream='1' WHERE id = $2", {
        $1: date,
        $2: jobId
    });
    return date;
};

JobDatastore.prototype.getAllUnscheduled = function (callback) {
    // do we want to limit?
    this.db.all("SELECT * FROM job WHERE gear_id is null", this._rowMapper(callback));
};

JobDatastore.prototype._rowMapper = function (callback) {
    var self = this;
    return function (error, rows) {
        if (Array.isArray(rows)) {
            var allJobs = rows.map(function (row) {
                return self._jobFromRow(row);
            });
            callback(allJobs);
        } else {
            callback(self._jobFromRow(rows));
        }
    }
};

JobDatastore.prototype._jobFromRow = function (row) {
    if (!row) {
        return undefined;
    }
    var streamAsInt = row.stream ? 1 : 0;
    var job = new Job(row.id, row.user_id, row.gear_def_id, row.file_size, row.name, streamAsInt);
    job.setGearId(row.gear_id);
    var scheduledDate = !row.scheduled_at ? undefined : new Date(row.scheduled_at).toISOString();
    job.setScheduledAt(scheduledDate);
    var completedDate = !row.completed_at ? undefined : new Date(row.completed_at).toISOString();
    job.setCompletedAt(completedDate);
    return job;
};

JobDatastore.prototype.getNumberUnscheduled = function (callback) {
    this.db.get("SELECT COUNT(*) AS c FROM job WHERE gear_id IS NULL", function (error, rows) {
        callback(rows.c);
    })
};

JobDatastore.prototype.getAllIncomplete = function (callback) {
    // do we want to limit?
    this.db.all("SELECT * FROM job WHERE completed_at IS NULL AND gear_id NOT NULL", this._rowMapper(callback));
};


module.exports = JobDatastore;