const GearDef = require('../model/gear_def.js');

/** @constructor
 * **/
function GearDefDatastore() {
    // add constants here - we could maybe read this in from a file?
    var ALL = [
        new GearDef("Urei", "1176", "Compressor", "http://www.dancetech.com/aa_dt_new/hardware/images/UREI_1176LN_original.jpg"),
        new GearDef("API", "550A", "EQ", "http://2.bp.blogspot.com/_k8FpKh5P4JY/Sv4GmnbL_II/AAAAAAAAA44/_H0RXDhd9iY/s400/Picture+44.png"),
        new GearDef("DBX", "530", "EQ", "https://www.soundtech.co.uk/images/products/dbx/dbx530-Front_original.jpg"),
        new GearDef("GML", "8200", "EQ", "http://www.global-audio-store.fr/1636/gml-8200-egaliseur-parametrique-5-bandes-2-canaux.jpg"),
        new GearDef("API", "2500", "Compressor", "http://apiaudio.com/img2x/products/prod_2500_1_m.jpg"),
        new GearDef("Lexicon", "PCM92", "Reverb", "http://smhttp.39666.nexcesscdn.net/801433B/vking/media/catalog/product/l/e/lexicon_pcm92_1.jpg")
    ];

    /**
     *
     * @returns {Array|*[]}
     */
    this.getAll = function () {
        return ALL;
    };

    /**
     *
     * @param {string} id
     * @returns {*}
     */
    this.getById = function (id) {
        for (var i = 0; i < ALL.length; i++) {
            if (ALL[i]._id === id) {
                return ALL[i];
            }
        }
        return null;
    }
}

module.exports = GearDefDatastore;
