/**
 *
 * @param db
 * @constructor
 */
function GearDatastore(db) {
    this.db = db;
    var dateString = db.getDateString();
    this.db.init([
        'CREATE TABLE IF NOT EXISTS gear (id TEXT PRIMARY KEY, studio_id TEXT, gear_def_id TEXT, name TEXT, address TEXT, fcm_token TEXT, stream BOOLEAN, updated_at ' + dateString + ')',
        'DELETE FROM gear'
    ]);
}

/**
 *
 * @param {Gear} gear
 */
GearDatastore.prototype.insertOrUpdate = function (gear) {
    var id = gear.getId();
    var date = new Date().toISOString();
    this.db.upsert("gear",
        ["id", "gear_def_id", "studio_id", "name", "address", "fcm_token", "stream", "updated_at"],
        [id, gear.gear_def_id, gear.studio_id, gear.name, gear.address, gear.fcm_token, gear.stream, date]);
};

GearDatastore.prototype.getGearById = function (gearId, callback) {
    this.db.get("SELECT * FROM gear WHERE id = '" + gearId + "'", function (err, row) {
        callback(row);
    });
};

GearDatastore.prototype.getLeastBusyGearForType = function (gearDefId, stream, limit, callback) {
    var streamInt = stream ? 1 : 0;
    var statement = "SELECT gear.id, gear.address, gear.fcm_token, gear.stream, a.s " +
        "FROM gear " +
        "LEFT OUTER JOIN (SELECT gear_id, gear_def_id, completed_at, SUM(file_size) as s FROM job " +
        "WHERE completed_at IS NULL AND gear_def_id = '" + gearDefId + "' " +
        "GROUP BY gear_id, gear_def_id, completed_at) a " +
        "ON a.gear_id = gear.id " +
        "WHERE gear.gear_def_id = '" + gearDefId + "' " +
        "AND gear.stream = '" + streamInt + "' " +
        "ORDER BY COALESCE(a.s, 0) ASC " +
        "LIMIT " + limit;
    this.db.all(statement, function (error, rows) {
        if (error) {
            console.log(error);
        }
        callback(rows);
    })
};

module.exports = GearDatastore;