/**
 * Created by GuyIntrepid on 11/23/16.
 */

const S3_BUCKET = process.env.S3_BUCKET || "dupontboyle";

/**
 *
 * @constructor
 */
function S3Filesystem() {
    var aws = require('aws-sdk');
    this.s3 = new aws.S3();
}

S3Filesystem.prototype.userDirectoryExists = function (userId) {
    // doesn't matter on aws
    return true;
};

S3Filesystem.prototype.createUserDirectory = function (userId) {
    // no-op
};

S3Filesystem.prototype.saveUploadedFile = function (userId, trackId, audioFile, callback) {
    const fileName = userId + "/" + trackId;
    const s3Params = {
        Bucket: S3_BUCKET,
        Key: fileName,
        Body: audioFile.data,
        ACL: 'authenticated-read'
    };
    this.s3.upload(s3Params, function (err, data) {
        callback(err);
    });
};

S3Filesystem.prototype.getJobReadStream = function (job, filenameFunction, callback) {
    var key = job.user_id + "/" + filenameFunction(job);
    var self = this;
    var options = {
        Bucket: S3_BUCKET,
        Key: key
    };
    // console.log(key);
    this.s3.headObject(options, function (err, data) {
        if (err) {
            callback(err)
        } else {
            callback(null, self.s3.getObject(options).createReadStream())
        }
    });
};

module.exports = S3Filesystem;