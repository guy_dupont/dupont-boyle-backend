/**
 * Created by GuyIntrepid on 11/23/16.
 */
const fileSystem = require('fs');

/**
 *
 * @constructor
 */
function LocalFileSystem() {
    var uploadsDir = __dirname + '/uploads/';
    if (!fileSystem.existsSync(uploadsDir)) {
        fileSystem.mkdirSync(uploadsDir);
    }
}

LocalFileSystem.prototype.userDirectoryExists = function (userId) {
    var userDir = __dirname + '/uploads/' + userId;
    return fileSystem.existsSync(userDir);
};

LocalFileSystem.prototype.createUserDirectory = function (userId) {
    var userDir = __dirname + '/uploads/' + userId;
    if (!fileSystem.existsSync(userDir)) {
        fileSystem.mkdirSync(userDir);
    }
};

LocalFileSystem.prototype.saveUploadedFile = function (userId, trackId, audioFile, callback) {
    var userDir = __dirname + '/uploads/' + userId;
    // callback will be called with err (or null) as first arg
    audioFile.mv(userDir + '/' + trackId, callback);
};

LocalFileSystem.prototype.getJobReadStream = function (job, filenameFunction, callback) {
    var filePath = __dirname + "/uploads/" + job.user_id + "/" + filenameFunction(job);
    fileSystem.exists(filePath, function (exists) {
        if (exists) {
            callback(null, fileSystem.createReadStream(filePath));
        } else {
            callback("File does not exist");
        }
    });
};

module.exports = LocalFileSystem;