/**
 *
 * @param {GearDefDatastore} gearDefDatastore
 * @param {WebInterface} webInterface
 * @param cloudNotifier
 */
module.exports = function start(gearDefDatastore, webInterface, cloudNotifier) {
    const Express = require('express');
    const FileUpload = require('express-fileupload');

    const SERVER_PORT = process.env.PORT || 5000;
    const IPV6_PREFIX = "::ffff:";
    const SERVER = Express();

    function generateCallback(webInterfaceFunction, paramCallback) {
        return function (request, response) {
            var extraParam;
            if (paramCallback) {
                extraParam = paramCallback(request);
            }
            webInterfaceFunction(function (responseCode, responseMessage) {
                response.status(responseCode).send(responseMessage);
            }, extraParam)
        }
    }

    SERVER.use(FileUpload());

    SERVER.get('/', generateCallback(webInterface.onRootRequest));

    SERVER.get('/gearDef', generateCallback(webInterface.getAllGearDef.bind(webInterface)));

    SERVER.get('/completedFile/:job_id', function (request, response) {
        downloadFileForJob(response, request.params.job_id, function (job) {
            return job.getAltId();
        })
    });

    SERVER.get('/nextJob/:gear_id', function (request, response) {
        webInterface.getNextJobForGear(function (responseCode, job, userFcmToken) {
            if (job) {
                if (job.stream) {
                    response.header("StreamAddress", userFcmToken);
                    cloudNotifier.sendStreamAddressToClient(userFcmToken, addressFromRequest(request));
                }
                downloadFileForJob(response, job.getId(), function (job) {
                    return job.getId();
                })
            } else {
                response.status(204).send();
            }
        }, request.params.gear_id);

    });

    function downloadFileForJob(response, jobId, fileNameFunction) {
        var requestObject = {job_id: jobId};
        webInterface.downloadFileForJob(function (responseCode, innerResponse) {
            if (responseCode != 200) {
                response.status(responseCode).send(innerResponse);
            } else {
                // might be problematic with s3
                response.attachment(jobId);
                innerResponse.pipe(response);
            }
        }, fileNameFunction, requestObject);
    }

    SERVER.get('/gearDef/:id', generateCallback(webInterface.getGearDefById.bind(webInterface),
        function (request) {
            return request.params.id;
        }
    ));

    SERVER.get('/jobsForUser/:user_id', generateCallback(webInterface.getAllJobsForUser.bind(webInterface),
        function (request) {
            return {
                user_id: request.params.user_id
            };
        }
    ));

    SERVER.post('/updateUserToken/:user_id', generateCallback(webInterface.updateUserToken.bind(webInterface),
        function (request) {
            var expressAddress = addressFromRequest(request);
            return {
                user_id: request.params.user_id,
                fcm_token: request.query.fcm_token,
                address: expressAddress
            };
        }
    ));

    SERVER.post('/streamJobCompleted/:job_id', generateCallback(webInterface.markStreamAsCompleted.bind(webInterface),
        function (request) {
            return {
                job_id: request.params.job_id,
            };
        }
    ));

    SERVER.post('/updateGearToken/:gear_id', generateCallback(webInterface.updateGearToken.bind(webInterface),
        function (request) {
            var expressAddress = addressFromRequest(request);
            return {
                gear_id: request.params.gear_id,
                fcm_token: request.query.fcm_token,
                address: expressAddress
            };
        }
    ));

    SERVER.post('/register', generateCallback(webInterface.insertOrUpdateGear.bind(webInterface),
        function (request) {
            var expressAddress = addressFromRequest(request);
            return {
                studio_id: request.query.studio_id,
                gear_def_id: request.query.gear_def_id,
                id: request.query.id,
                fcm_token: request.query.fcm_token,
                name: request.query.name,
                address: expressAddress
            }
        }));

    SERVER.post('/uploadCompleted/:job_id', generateCallback(webInterface.uploadCompleted.bind(webInterface),
        function (request) {
            return {
                job_id: request.params.job_id,
                audio: request.files ? request.files.audio : null
            }
        }
    ));

    SERVER.post('/upload', generateCallback(webInterface.uploadAudio.bind(webInterface),
        function (request) {
            var expressAddress = addressFromRequest(request);
            return {
                name: request.query.name,
                user_id: request.query.user_id,
                gear_def_id: request.query.gear_def_id,
                fcm_token: request.query.fcm_token,
                stream: request.query.stream || false,
                audio: request.files ? request.files.audio : null,
                address: expressAddress
            }
        }
    ));

    function addressFromRequest(request) {
        var ip = request.headers['x-forwarded-for'] ||
            request.connection.remoteAddress ||
            request.socket.remoteAddress ||
            request.connection.socket.remoteAddress;
        if (ip.startsWith(IPV6_PREFIX)) {
            ip = ip.replace(IPV6_PREFIX, '');
        }
        if (ip.indexOf(", ") >= 0) {
            ip = ip.split(", ")[0];
        }
        return ip;
    }

// Make sure all of your API routes are in place before calling listen()
    SERVER.listen(SERVER_PORT, function (err) {
        if (err) {
            return console.log('something bad happened', err);
        }
        console.log(`server is listening on ${SERVER_PORT}`);
    });
};

