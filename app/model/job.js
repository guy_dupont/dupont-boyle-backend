const BaseModel = require('./base_model');
/**
 *
 * @param {string} userId
 * @param {string} gearDefId
 * @param {string} id
 * @param {number} fileSize
 * @param {string} name
 * @param stream
 * @constructor
 */
function Job(id, userId, gearDefId, fileSize, name, stream) {
    BaseModel.call(this, id);
    this.user_id = userId;
    this.gear_def_id = gearDefId;
    this.file_size = fileSize;
    this.name = name;
    this.stream = stream || 0;
}

Job.prototype = Object.create(BaseModel.prototype);

Job.prototype.setGearId = function (gearId) {
    this.gear_id = gearId;
};

Job.prototype.setScheduledAt = function (date) {
    this.scheduled_at = date;
};

Job.prototype.setCompletedAt = function (date) {
    this.completed_at = date;
};

module.exports = Job;
