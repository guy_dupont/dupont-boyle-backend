function BaseModel(id) {
    this._id = id;
}

BaseModel.prototype.getId = function () {
    return this._id;
};

BaseModel.prototype.getAltId = function () {
    return "_" + this._id;
};

module.exports = BaseModel;