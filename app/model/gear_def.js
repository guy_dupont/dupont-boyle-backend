var BaseModel = require('./base_model');
/**
 *
 * @param {string} make
 * @param {string} model
 * @param {string} type
 * @constructor
 */
function GearDef(make, model, type, imageUrl) {
    BaseModel.call(this, make + model);
    this.make = make;
    this.model = model;
    this.type = type;
    this.image_url = imageUrl;
}

GearDef.prototype = Object.create(BaseModel.prototype);

module.exports = GearDef;
