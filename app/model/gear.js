var BaseModel = require('./base_model')
/**
 *
 * @constructor
 */
function Gear(id, studio_id, gear_def_id, name, fcm_token, stream, address) {
    BaseModel.call(this, id);
    this.studio_id = studio_id;
    this.gear_def_id = gear_def_id;
    this.fcm_token = fcm_token;
    this.name = name;
    this.stream = stream;
    this.address = address;
}

Gear.prototype = Object.create(BaseModel.prototype);

module.exports = Gear;
