/**
 * Created by GuyIntrepid on 11/5/16.
 */
const Rx = require('rx');

const LOOP_INTERVAL = 10000;
const MAX_UNSCHEDULED_COUNT = 3;
/**
 *
 * @param {JobDatastore} jobDatastore
 * @param {GearDatastore} gearDatastore
 * @param cloudNotifier
 * @constructor
 */
function Scheduler(jobDatastore, gearDatastore, cloudNotifier) {
    var self = this;
    this.jobDatastore = jobDatastore;
    this.udpNotifier = cloudNotifier;
    this.subject = new Rx.Subject();
    var subjectObs = this.subject
        .flatMap(() => Rx.Observable.fromCallback(jobDatastore.getNumberUnscheduled.bind(jobDatastore))())
        .filter(count => count < MAX_UNSCHEDULED_COUNT);
    var obs = Rx.Observable
        .interval(LOOP_INTERVAL)
        .timeInterval();
    this.chain = Rx.Observable
        .merge(subjectObs, obs)
        .flatMap(() => Rx.Observable.fromCallback(jobDatastore.getAllUnscheduled.bind(jobDatastore))())
        .flatMap(list => Rx.Observable.from(list))
        .flatMap(function (job) {
            return Rx.Observable.fromCallback(gearDatastore.getLeastBusyGearForType.bind(gearDatastore))(job.gear_def_id, job.stream, 5);
        }, function (job, gearList) {
            return self.schedule(job, gearList);
        })
    ;
}

Scheduler.prototype.schedule = function (job, gearList) {
    if (gearList.length) {
        var gear = gearList[0];
        var gearId = gear.id;
        var date = this.jobDatastore.markAsScheduled(job.getId(), gearId);
        job.setGearId(gearId);
        job.setScheduledAt(date);
        this.udpNotifier.pokeGear(gear)
    } else {
        //TODO none of this type
    }
    return job;
};

Scheduler.prototype.start = function () {
    this.chain.subscribe();
};

Scheduler.prototype.poke = function () {
    this.subject.onNext(1);
};

module.exports = Scheduler;