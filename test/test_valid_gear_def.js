// get enum from schema for validating at test time
const allTypes = require('../app/schema/gear_def.json').properties.type.enum;
const GearDefDatastore = require('../app/datastore/gear_def_datastore.js');
const gearDefDatastore = new GearDefDatastore();
const allDefs = gearDefDatastore.getAll();
const ids = [];
for (var i = 0; i < allDefs.length; i++) {
    if (ids.indexOf(allDefs[i].type) >= 0) {
        throw "Error - duplicate gear defs";
    }
    if (allTypes.indexOf(allDefs[i].type) < 0) {
        throw "Error - invalid gear def type"
    }
    ids.push(allDefs.type);
}

var fetchedById = gearDefDatastore.getById("Urei1176");
if (fetchedById == null || fetchedById.make !== "Urei") {
    throw "Error - gear def fetchById failed";
}
