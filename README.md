# dupont-boyle-backend
### Setting Up
* [Follow this Guide](https://blog.risingstack.com/node-hero-tutorial-getting-started-with-node-js/) to install Node and get a sense for how to start a project and add dependencies.
* Note - this project was built using v6.9.1.  The guide is a little out of date here.
* If it is your first time running the server, or your dependencies are otherwise out of date, run `npm install` to get the latest.
* While in the project directory, executing `npm start` will load/run our `index.js` file, which only exists to run our application's "main" file - `/app/index.js`.  This is where the server starts.
* Similarly, executing `npm test` will kick off everything in `test.js`
* Once the server is running on a given port, you should be able to hit any of the requests you defined in `/app/index.js` at `localhost:{port_number}` using Postman, curl, or your browser.

### Generating POJOs from JSON Schema
* Install `jsonschema2pojo` - `brew install jsonschema2pojo`
* From the root project directory, run `jsonschema2pojo -s ./app/schema -t ./app/pojo_output -a GSON -R -E -S`

### Dependencies
* [Express](https://www.npmjs.com/package/express) - Easily define and spin up an HTTP server.
* [RX](https://www.npmjs.com/package/rx) - Reactive Expressions library for js.
* [express-jsonschema](https://www.npmjs.com/package/express-jsonschema) - For validating Express requests against JSON Schema.
* [UUID](https://www.npmjs.com/package/uuid) - For generating UUID's.
* [sqlite3](https://www.npmjs.com/package/sqlite3) - For running with a local SQLite database.
* [pg](https://www.npmjs.com/package/pg) - For running with a PostgreSQL database.
* [fcm-node](https://www.npmjs.com/package/fcm-node) - Simplified Firebase Cloud Messaging integration.
* [aws-sdk](https://www.npmjs.com/package/aws-sdk) - Library for AWS integration.

### API
`GET /gearDef`

* Get a list of the accepted types of gear.

`GET /gearDef/:id`

* Get more detail about a particular type of gear.

`GET /completedFile/:job_id`

* Request to download the final processed audio file for a particular job.

`GET /file/:gear_id`

* Request to download the next audio file queued up for a piece of gear.

`GET /jobsForUser/:user_id`

* Get a list of all jobs for a given user.  Includes both completed and incomplete.

`POST /register [studio_id, gear_def_id, id, name]`

* Registers the device that makes this request as a piece of gear (by IP).
    * studio_id - Identifier for the studio that owns this gear.
    * gear_def_id - Type of gear.  Must correspond to one of the options returned by `/gearDef`.
    * fcm_token - Firebase Cloud Messaging token associated with this gear.
    * id (optional) - Specify a pre-existing id if you need to re-use a device on a different IP address.
    * name (optional) - Give this device a human-readable name.

`POST /upload [gear_def_id, user_id, name, stream]`

* Upload a file to be processed.  Should only be called by a customer's device.
    * gear_def_id - Type of gear that this file should be run through. Must correspond to one of the options returned by `/gearDef`.
    * user_id - Current user's id.
    * fcm_token - Firebase Cloud Messaging token to be associated with the user uploading this job.
    * name (optional) - Name this track.
    * stream (optional) - Boolean indicating whether or not this is a streaming job or normal job.  Defaults to false (normal).

`POST /uploadCompleted/:job_id`

* Upload the final processed file for a given job.  Should only be called by a studio device.

`POST /streamJobCompleted/:job_id`

* Mark this job as done.  Since it was a stream, there is no file to upload.

`POST /updateGearToken/:gear_id`

* Update the Firebase Cloud Messaging token associated with gear specified by gear_id.
    * fcm_token - Firebase Cloud Messaging token.
    
`POST /updateUserToken/:user_id`

* Update the Firebase Cloud Messaging token associated with a user.
    * fcm_token - Firebase Cloud Messaging token.


