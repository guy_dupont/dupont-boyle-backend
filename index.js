// index.js
const GearDefDatastore = require('./app/datastore/gear_def_datastore');
const GearDatastore = require('./app/datastore/gear_datastore');
const UserDatastore = require('./app/datastore/user_datastore');
const JobDatastore = require('./app/datastore/job_datastore');
const SqliteWrapper = require('./app/datastore/sqlite_wrapper');
const PgWrapper = require('./app/datastore/pg_wrapper');
const WebInterface = require('./app/web_interface');
const UdpNotifier = require('./app/messaging/udp_notifier');
const FcmNotifier = require('./app/messaging/fcm_notifier');
const Scheduler = require('./app/scheduler');
const LocalFileSystem = require('./app/local_filesystem');
const S3Filesystem = require('./app/s3_filesystem');

var db;
var fileSystem;
// var dbUrl = "postgres://guqtuigipadnmt:yBq-Xxlzx9sL52mRHqMvFVOyi8@ec2-54-243-249-37.compute-1.amazonaws.com:5432/deuvbfdbgnfa6s";
var dbUrl = process.env.DATABASE_URL;
if (!dbUrl) {
    var sqlite3 = require('sqlite3').verbose();
    var file = 'database.db';
    fileSystem = new LocalFileSystem();
    db = new sqlite3.Database(file, function () {
        console.log("STARTING WITH LOCAL DB");
        db = new SqliteWrapper(db);
        startApp();
    });
} else {
    const pg = require('pg');
    pg.defaults.ssl = true;
    // temp
    fileSystem = new S3Filesystem();
    //fileSystem = new LocalFileSystem();
    pg.connect(dbUrl, function (err, client) {
        if (err) throw err;
        console.log('Connected to postgres! Getting schemas...');
        db = new PgWrapper(client);
        startApp();
    });
}



function startApp() {
    const gearDefDatastore = new GearDefDatastore();
    const jobDatastore = new JobDatastore(db);
    const gearDatastore = new GearDatastore(db);
    const userDatastore = new UserDatastore(db);
    const cloudNotifier = new FcmNotifier(userDatastore, jobDatastore);
    const scheduler = new Scheduler(jobDatastore, gearDatastore, cloudNotifier);
    const webInterface = new WebInterface(gearDefDatastore, gearDatastore, jobDatastore, userDatastore, cloudNotifier, fileSystem, scheduler);
    scheduler.start();
    require('./app/index')(gearDefDatastore, webInterface, cloudNotifier);
}
